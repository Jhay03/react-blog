import React, {useState} from 'react';
import EditCommentForm from './forms/EditCommentForm';

const CommentsItem = ({ comments, postId, editComment, deleteComment }) => {
    const [editing, setEditing] = useState({
        commentId: null,
        edit:false
    })
    const showComments = comments.map(comment => {
        if (comment.postId === postId) {
            return (
                <div key={comment.id}>
                    {editing.edit && editing.commentId == comment.id ?
                        <EditCommentForm
                        comment={comment}
                            setEditing={setEditing}
                            editComment={editComment}
                    /> :
                        <div>
                            <p>{comment.body}</p> 
                            <button onClick={() => setEditing({commentId: comment.id, edit:true})}>Edit</button>
                            <button onClick={() => deleteComment(comment.id)}>Delete</button>
                        </div>
                    }
                    
                    
                    
                </div>
            )
        }
    })

    return (
        <div>
            {showComments}
        </div>
    )
}

export default CommentsItem;