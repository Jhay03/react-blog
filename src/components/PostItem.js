import React, {useState} from 'react';
import EditForm from './forms/EditForm';
import CommentForm from "./forms/CommentForm";
import CommentsItem from "./CommentsItem";

const PostItem = (
    {
        post: { title, body, userId, id },
        editPost,
        deletePost,
        addComment,
        comments,
        editComment,
        deleteComment
    }
    ) => {
    //console.log(props)
    const [editing, setEditing] = useState(false)

    const style = {
        background: "#eee",
        padding: "10px",
        marginBottom: "10px",
        border: "2px solid black"
    }
    return (
        <div style={style}>
            
            {editing ?
                <EditForm
                    setEditing={setEditing}
                    title={title}
                    body={body}
                    postId={id}
                    editPost={editPost}
                    deletePost={deletePost}
                /> :
                <div>
                    <small>{id}</small>
                    <h2>{title}</h2>
                    <p>{body}</p>
                    <small>Posted by: {userId}</small>
                    <button onClick={() => setEditing(true)}> Edit </button>
                    <button onClick={() => deletePost(id)}> Delete</button>
                   
                </div>
               
            }
             <CommentForm
                addComment={addComment}
                postId={id}
            />
            <CommentsItem
                comments={comments}
                postId={id}
                editComment={editComment}
                deleteComment={deleteComment}
            />
            {/*{comments.map(comment =>  <CommentsItem />)}*/}
            
        </div>
    )
}
 
export default PostItem;