import React, {useState} from 'react';

const EditCommentForm = ({ comment, setEditing, editComment }) => {
    const [updatedComment, setUpdatedComment] = useState({
        id: comment.id,
        postId: comment.postId,
        userId: comment.userId,
        body: comment.body
    })
    const onChangeHandler = (e) => {
        setUpdatedComment({
            ...updatedComment,
            body: e.target.value
            
        })
    }
    console.log(updatedComment)
    return (
        <div>
            <h1>Edit Your Comment</h1>
            <textarea 
                cols="50"
                row="10"
                value={updatedComment.body}
                name="body"
                onChange={onChangeHandler}
            />
            
            <button onClick={() =>
            {
                setEditing(false)
                editComment(updatedComment)
            }}>Submit
            </button>
            <button onClick={() => setEditing(false)}>Cancel</button>
        </div>
    )
}

export default EditCommentForm;