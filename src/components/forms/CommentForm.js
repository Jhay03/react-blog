import React, {useState} from 'react';



const CommentForm = ({ addComment, postId }) => {
    const [comment, setComment] = useState("")
    const onChangeHandler = (e) => {
        setComment(e.target.value)
    }
    return (
        <div>
            <h1>Comment Form</h1>
            <textarea placeholder="Write your Comment . . . "
                cols="50"
                row="10"
                onChange={onChangeHandler}
            />
            
            <p>{comment}</p>
            <br/>
            <button onClick={() => addComment(postId, comment)}>Add Comment</button>
        </div>
    )
}

export default CommentForm;