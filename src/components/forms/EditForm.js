import React, {useState} from 'react';

const EditForm = ({ setEditing, title, body, id, editPost }) => {
    const [formData, setFormData] = useState({
        id,
        title,
        body

    })
    const onChangeHandler = (e) => {
        setFormData({
            ...formData,
            [e.target.name]: e.target.value
        })
    }

    //console.log(formData);
    return (
        <div>
            <h2>Editing</h2>
            <label htmlFor="title">Title</label>
            <input
                type="text"
                id="title"
                name="title"
                value={formData.title}
                onChange={onChangeHandler}
            />
            <br />
            <label htmlFor="body">Body</label>
            <input
                type="text"
                id="body"
                name="body"
                value={formData.body}
                onChange={onChangeHandler}
            />
            <button
                onClick={() => {
                    editPost(formData)
                    setEditing(false)
                }}
            >Submit</button>
            <br />
            <br />
           <button onClick={() => setEditing(false)}>Cancel</button>
            
            
            <br />
        </div>
    )
}

export default EditForm;