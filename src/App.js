import React, { useState, useEffect } from 'react';
import PostItem from "./components/PostItem"
import AddForm from "./components/forms/AddForm";
import { v4 as uuidv4 } from "uuid";

function App() {
  const [posts, setPosts] = useState([]);
  const [loading, setLoading] = useState(true)
  const [showForm, setShowForm] = useState(false)
  const [comments, setComments] = useState([])
  useEffect(() => {
    fetch("https://jsonplaceholder.typicode.com/posts?_limit=10") 
      .then(res => res.json()) //pasok sa response na variable and convert to json format
      .then(data => {
        setLoading(false)
        data.map(post => {
         return post.comments = comments
        })
        setPosts(data.slice(0, 2)) //limit the value of fetch to 2
      }) //then store to variable data
   // alert("Hello World")
  }, [comments])
  //console.log(posts)
  //console.log(comments)
  
  //adding post
  const addPost = (formData) => {
   // alert('This is the add post function')
   // console.log(formData)
    setPosts([
      ...posts,
      {
        userId: 1,
        id: uuidv4(),
        title: formData.title,
        body: formData.body
      }
    ])
  }

    //updating post
  const editPost = ({id,title,body}) => {
    //const updatedPost = posts.map(post => {
     //return post.id === postToBeEdited.id ? {...post, titlee, body}  : 
  /// })
    const updatedPosts = posts.map(post => {
      return post.id === id ? { ...post, title, body} : post
    })
    setPosts(updatedPosts)
  // console.log(postToBeEdited);
    //alert ("This is the edit post")
  }

  //deleting post
  const deletePost = (postId) => {
    let result = window.confirm("are you sure want to delete??")
    if(result){
    const updatedPosts = posts.filter(post => post.id !== postId)
    setPosts(updatedPosts)
    }
  }

  //adding comments 
  const addComment = (id, comment) => {
  // alert(id + " " + comment)
    const newComment = { 
      id: uuidv4(),
      postId: id,
      userId: 1,
      body: comment
    }
    setComments([...comments, newComment])
  }


  const editComment = (updatedComment) => {
   // alert ("Successfully Edited")
  //alert(JSON.stringify(updatedComment))
    
    const updatedComments = comments.map(comment => {
      return comment.id === updatedComment.id ?
        { ...comment, body: updatedComment.body } :
        comment;
    })
    setComments(updatedComments)
  }

  
  
  //deleting post
  const deleteComment = (commentId) => {
    let result = window.confirm("are you sure want to delete??")
    if(result){
    const updatedComments = comments.filter(comment => comment.id !== commentId)
    setComments(updatedComments)
    }
  }
  

 /* const loadingStyle = {
    height: "100vh",
    width: "100%",
    bacgroundSize: "cover"
  }*/

  //if the posts is not empty ? do this : else do this
  const allPosts = posts.length ?

    posts.map(post => (
   
      <PostItem
        key={post.id}
        post={post}
        editPost={editPost}
        deletePost={deletePost}
        addComment={addComment}
        comments={comments}
        editComment={editComment}
        deleteComment={deleteComment}
      />
      
    )).reverse()

    : <h1>No Post To Show</h1>

  const isLoading = loading ? /*<img src="https://i.gifer.com/1zes.gif" alt="gift" style={loadingStyle}/>*/ <h1>Loading . . .</h1> : allPosts

  return (
    <div className="App" >
      {showForm ?
        <AddForm
          addPost={addPost}
        /> :
        <button onClick={() => setShowForm(true)}>
          Add Post
          </button>}
      {isLoading}
    </div>
  );
}

export default App;